!define NAME "NaturalScroll"
!define VER "1.0.0.0"

;=== Program Details
Name "${NAME}"
OutFile "${NAME}.exe"
Caption "${NAME}"
VIProductVersion "${VER}"
VIAddVersionKey ProductName "${NAME}"
VIAddVersionKey Comments "Set the magic registry settings for natural scrolling."
VIAddVersionKey FileDescription "${NAME}"
VIAddVersionKey FileVersion "${VER}"
VIAddVersionKey ProductVersion "${VER}"
VIAddVersionKey InternalName "${NAME}"
VIAddVersionKey OriginalFilename "${NAME}.exe"
VIAddVersionKey LegalCopyright "WTFPL"

;=== Runtime Switches
CRCCheck Off
WindowIcon Off
SilentInstall Silent
AutoCloseWindow True
RequestExecutionLevel admin

!include Registry.nsh

; Icon "appicon.ico"

Section "Main"
	
	${registry::Open} "HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Enum\HID" "/K=1 /V=0 /G=1 /N='Device Parameters'" $R0
	StrCmp $R0 "0" 0 RegOkay

	MessageBox MB_OK|MB_ICONEXCLAMATION "reg::Open failed"
	Abort

RegOkay:
	${registry::Find} $R0 $R1 $R2 $R3 $R4
	StrCmp $R4 "" Done
	; MessageBox MB_OK|MB_ICONEXCLAMATION "$R1\$R2, FlipFlopWheel"
	${registry::Write} "HKEY_LOCAL_MACHINE\$R1\$R2" "FlipFlopWheel" "1" "REG_DWORD" $R4
	StrCmp $R4 "0" 0 Err
	${registry::Write} "HKEY_LOCAL_MACHINE\$R1\$R2" "FlipFlopHScroll" "1" "REG_DWORD" $R4
	StrCmp $R4 "0" RegOkay Err
	; goto RegOkay

Err:
	MessageBox MB_OK|MB_ICONEXCLAMATION "reg::Write failed"
	Abort
	
Done:
	MessageBox MB_OK|MB_ICONEXCLAMATION "Done, scrolling will be natural on your next reboot"
	${registry::Unload}
	
SectionEnd
