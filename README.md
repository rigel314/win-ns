win-ns
======

# About
I got tired of flipping the registry settings for each mouse every time windows updates or everytime I plug a new mouse in.  So I made this thing with NSIS to solve my problem.  NSIS is a language for making installers and is also used for making portable app wrappers.  I used to play around with portable app wrappers which had to do registry operations like (rename existing registry keys, write saved keys, save off keys, restore original existing keys).  

# How it works
It finds all of the registry keys in `HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Enum\HID` called `Device Parameters` then sets the `FlipFlopWheel` and `FlipFlopHScroll` values for each key to `1`.

# Usage
Run the exe from CI to set all of the FlipFlop values to 1 whenever you plug in a new mouse or update windows.  The changed setting will not take effect until you reboot. (or maybe just log out)
